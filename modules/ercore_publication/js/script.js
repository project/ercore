/**
* @file
*/

// example function
// (function ($, Drupal) {
// Drupal.AjaxCommands.prototype.fetch = function (ajax, response, status) {
//   console.log(response.message);
// }

// })(jQuery, Drupal);




Drupal.behaviors.doi = {
  attach: (function ($, context, settings) {
    // var userCite = $("#edit-field-ercore-pb-full-citation-0-value").val(); //TODO dont need this particularly
    $(document).ready(function(){
      console.log("TESTTEST")
      if ($('#edit-field-previous-federal-repo--wrapper > legend').length){
        
        $( "#edit-field-previous-federal-repo--wrapper > legend" ).append( $( '<span class="tooltip"><div class=" tooltip-style">?<div class="tooltiptext tooltiptext2"><div>The National Science Foundation (NSF) has developed interoperability with another Federal agency, the Department of Energy, Office of Scientific and Technical Information (DOE/OSTI) in order to allow authors of publications funded by both agencies (NSF and DOE) to deposit the findal accepted version fo their manuscript one time. Authors who have successfully deposited and eligible publication in DOE/OSTI’s system will now be able to seamlessly make their publication available through NSF-PAR by choosing “Yes” and selecting the appropriate entry for the federal repository from the drop-down.</div><br><div>If you have deposited a publication in the DOE system but you’re not able to successfully upload using the option provided, indicate “No”</div></div></div><span style="margin-left: 4px; font-size: 7pt; font-weight: 500;">More Info</span></span>' ) );
        $( "#edit-field-federal-repository-wrapper > div " ).append( $( '<span class="tooltip"><div class=" tooltip-style">?<div class="tooltiptext tooltiptext2"> Currently, the NSF-PAR system only supports the Department of Energy (DOE) federal repository. If you have deposited your publication in a repository other than DOE, please select the “No” option above. </div></div><span style="margin-left: 4px; font-size: 7pt;">What if the Federal Repository is not listed here?</span></span>' ) );

      }
      $("#edit-field-doi-json-wrapper").hide();
      if($("#edit-field-ercore-pb-doi-0-value").val() != "" && $("#edit-field-doi-json-0-value").val() == ""){
        $.ajax({
          url: "https://dx.doi.org/" + $("#edit-field-ercore-pb-doi-0-value").val(), dataType: "json",
          success: function (response) {
            $("#edit-field-doi-json-0-value").val(JSON.stringify(response));
            console.log($("#edit-field-doi-json-0-value").val());
          }
        })
      }
    });
    // Attach a click listener to the edit button.
    var clearBtn = document.getElementById('edit-button');
    var doiResponse = null;
    clearBtn.addEventListener('click', function () {

      // Do something!
      doi = $("#edit-doitext").val();
      typeMap = {
        "article-journal": "Journal Article",
        "book": "Book",
        "paper-conference": "Conference Proceedings",
        "article-magazine": "Magazine Article",
        "report": "Technical Report",
        "article-newspaper": "Newspaper Article"
      }
      $.ajax({
        url: "https://dx.doi.org/" + doi, dataType: "json",
        success: function (response) {
          doiResponse = response;
          window.doiResponse = doiResponse; //TODO not so sure it need this
          entities = {
            "title": $("#edit-title-0-value").val(response.title),
            "container-title": $("#edit-field-ercore-pb-journal-name-0-value").val(response["container-title"]),
            "volume": $("#edit-field-ercore-pb-journal-volume-0-value").val(response.volume),
            "publisher": $("#edit-field-ercore-pb-publisher-0-value").val(response.publisher),
            "page": $("#edit-field-ercore-pb-journal-pages-0-value").val(response.page),
            "DOI": $("#edit-field-ercore-pb-doi-0-value").val(response["DOI"]),
            "type": $("#edit-field-ercore-pb-type").val(typeMap[response.type]),
            // "abstract" : $("edit-body-0-value").val(response.abstract),//TODO
          }
          // console.log(entities);
          $.each(entities, function (index, value) {
            if (response.hasOwnProperty(index) == true) {
              // console.log(value);
              value;
            }

          });
          console.log(response);
          var strJSON = JSON.stringify(response);
          $("#edit-field-doi-json-0-value").val(strJSON);
          $("#edit-field-ercore-pb-status").val("0");
          $("#edit-field-ercore-pb-date-wrapper").show();
          $("#edit-field-ercore-pb-submit-date-wrapper").show();
          $("#edit-field-ercore-nsf-wrapper").show();
          $("#edit-field-ercore-pb-journal-name-wrapper").show();
          $("#edit-field-ercore-pb-journal-volume-wrapper").show();
          $("#edit-field-ercore-pb-publisher-wrapper").show();
          $("#edit-field-ercore-pb-coll-title-wrapper").show();
          $("#edit-field-ercore-pb-journal-pages-wrapper").show();
          $("#edit-field-ercore-pb-doi-wrapper").show();
          $("#edit-field-ercore-pb-issn-wrapper").show();
          $("#edit-field-ercore-pb-ark-wrapper").show();
          $("#edit-body-wrapper").show();
          // $("#edit-body-0-value").show();

          // $("#edit-field-ercore-pb-full-citation-0-value").val("EMPTY"); // TODO remove this
            author_list = "";
            citation_author_list = "";
            $.each(doiResponse.author, function (index, this_author) {
              console.log(doiResponse.author.length);
              if(index == doiResponse.author.length - 1 || doiResponse.author.length == 2){
                citation_author_list += " & ";
              } else if(index != doiResponse.author.length-1 && index != 0){
                citation_author_list += ", ";
              }
              citation_author_list = citation_author_list + this_author["family"] + ", " + this_author['given'].charAt(0) + ".";

            });
            yearpub = ""
            if (doiResponse.hasOwnProperty("published-print")) {
              yearpub = doiResponse["published-print"]["date-parts"][0][0]
            } else {
              yearpub = doiResponse["published-online"]["date-parts"][0][0]
            }
            citation_author_list = citation_author_list + " (" + yearpub + "). " + doiResponse["title"] + ". ";
            if(doiResponse.type == "article-journal" || doiResponse.type == "article-newspaper" || doiResponse.type == "article-magazine"){
              citation_author_list += doiResponse["container-title"] + ", " + doiResponse.volume + ", ";
            }
            if (doiResponse.hasOwnProperty("page")) {
              citation_author_list += doiResponse.page + ". ";
            }
            citation_author_list += "DOI: " + doiResponse["DOI"];
            $("#edit-field-ercore-pb-full-citation-0-value").val(citation_author_list);

          function citation() { // TODO is this needed?
            var citStr = "";
            $.each(response.author, function (index, value) {
              $.each(value, function (obj, val) {
                if (obj == "given") {
                  citStr += val + ", ";
                }
                if (obj == "family") {
                  citStr += val.charAt(0);
                  if (val.charAt(-1) == ".") {

                  }
                }
              });
            });
          }

          function issbn() {
            count = 0;
            if (response.hasOwnProperty("ISSN")) {
              count += 1;
            }
            if (response.hasOwnProperty("ISBN")) {
              count += 2;
            }
            return count;
          }
          switch (issbn()) {
            case 0:
              break;
            case 1:
              $("#edit-field-ercore-pb-issn-0-value").val(response.ISSN);
              break;
            case 2:
              $("#edit-field-ercore-pb-issn-0-value").val(response.ISBN);
              break;
            case 3:
              $("#edit-field-ercore-pb-issn-0-value").val(response.ISSN + "/" + response.ISBN);
          }

          pubdate = new Date(response["created"]["date-time"])
          var pubdate_day = ('0' + pubdate.getDate()).slice(-2);
          var pubdate_month = ('0' + (pubdate.getMonth() + 1)).slice(-2); //Months are zero based
          var pubdate_year = pubdate.getFullYear();
          pub_str = pubdate_year + "-" + pubdate_month + "-" + pubdate_day;
          $("#edit-field-ercore-pb-date-0-value-date").val(pub_str);
          $("#edit-field-ercore-pb-link-0-uri").val(response["URL"])


          function hasDates() {
            $("#edit-field-ercore-pb-status").val("0");
            $("#edit-field-ercore-pb-date-wrapper").show();
            $("#edit-field-ercore-pb-submit-date-wrapper").show();
            $("#edit-field-ercore-nsf-wrapper").show();
            if (response.hasOwnProperty("published-print") && response.hasOwnProperty("published-online")) {
              return "both";
            }
            else if (response.hasOwnProperty("published-print")) {
              return "print";
            }
            else if (response.hasOwnProperty("published-online")) {
              return "online"
            }
            else {
              return null;
            }
          }

          var pubType = ["published-online", "published-print"];
          pubMap = {
            "published-online": {
              "obj": null,
              "str": ""
            },
            "published-print": {
              "obj": null,
              "str": ""
            }
          }

        }
      });

    }, false);
    // add change listenter to citation format list
    var citeFormatList = document.getElementById('edit-field-citation-format');
    citeFormatList.addEventListener('change', function(){
      console.log($('#edit-field-citation-format').val());
      // var selectedVal = $(this).find(':selected').val(); //alternate way of getting same result as selection = $('#edit-field-citation-format').val();
      // var selectedText = $(this).find(':selected').text();
      selection = $('#edit-field-citation-format').val();
      console.log(selection)
      var citation = "";
      if(doiResponse != null || $("#edit-field-doi-json-0-value").val() != ""){
        if($("#edit-field-doi-json-0-value").val() != ""){
          console.log($("#edit-field-doi-json-0-value").val());
          var objJSON = JSON.parse($("#edit-field-doi-json-0-value").val());
          doiResponse = objJSON;
        }
        switch (selection){
          case '_none':
            $("#edit-field-ercore-pb-full-citation-0-value").val("");
            break;
          case 'APA':
            // $("#edit-field-ercore-pb-full-citation-0-value").val("EMPTY"); // TODO remove this
            author_list = "";
            citation_author_list = "";
            $.each(doiResponse.author, function (index, this_author) {
              console.log(doiResponse.author.length);
              if(index == doiResponse.author.length - 1 || doiResponse.author.length == 2){
                citation_author_list += " & ";
              } else if(index != doiResponse.author.length-1 && index != 0){
                citation_author_list += ", ";
              }
              citation_author_list = citation_author_list + this_author["family"] + ", " + this_author['given'].charAt(0) + ".";

            });
            yearpub = ""
            if (doiResponse.hasOwnProperty("published-print")) {
              yearpub = doiResponse["published-print"]["date-parts"][0][0]
            } else {
              yearpub = doiResponse["published-online"]["date-parts"][0][0]
            }
            citation_author_list = citation_author_list + " (" + yearpub + "). " + doiResponse["title"] + ". ";
            if(doiResponse.type == "article-journal" || doiResponse.type == "article-newspaper" || doiResponse.type == "article-magazine"){
              citation_author_list += doiResponse["container-title"] + ", " + doiResponse.volume + ", ";
            }
            if (doiResponse.hasOwnProperty("page")) {
              citation_author_list += doiResponse.page + ". ";
            }
            citation_author_list += "DOI: " + doiResponse["DOI"];
            $("#edit-field-ercore-pb-full-citation-0-value").val(citation_author_list);
            break;
          case 'Chicago':
            //Authors List
            author_list = ""
            citation_author_list = ""
            $.each(doiResponse.author, function (index, this_author) { //TODO
              author_list = author_list + this_author['given'] + " " + this_author['family'] + "\n"

              if (this_author['sequence'] == "first") {
                citation_author_list = citation_author_list + this_author['family'] + ", " + this_author['given']
              } else {
                citation_author_list = citation_author_list + ", " + this_author['given'] + " " + this_author['family']
              }
            });
            yearpub = ""
            if (doiResponse.hasOwnProperty("published-print")) {
              yearpub = doiResponse["published-print"]["date-parts"][0][0]
            } else {
              yearpub = doiResponse["published-online"]["date-parts"][0][0]
            }
            page = ""
            if (doiResponse.hasOwnProperty("page")) {
              page = ", p" + doiResponse["page"]
            }
            citation_author_list += " " + yearpub + ". \"" + doiResponse["title"] + ".\" " + doiResponse["container-title"] + " " + doiResponse["volume"] + "(" + doiResponse["issue"] + ")" + page + ". " + doiResponse["publisher"] + ". doi:" + doiResponse["DOI"]

            $("#edit-field-ercore-pb-full-citation-0-value").val(citation_author_list);
            break;
        }
          $.each(doiResponse.author, function (index, this_author) {
            console.log(this_author);
          });
      }
    });
  }(jQuery))
};