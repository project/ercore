<?php

namespace Drupal\ercore_publication\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Class DoiPopulation.
 */
class DoiPopulation implements CommandInterface {

  /**
   * Render custom ajax command.
   *
   * @return ajax
   *   Command function.
   */
  public function render() {
    return [
      'command' => 'fetch',
      'message' => 'My Awesome Message',
    ];
  }

}
