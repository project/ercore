<?php

namespace Drupal\ercore_core;

/**
 * Class ErcoreCollaboration.
 *
 * @package Drupal\ercore_core
 */
class ErcoreCollaboration {
  public $type = '';
  public $localInstitutions = '0';
  public $localCollaborators = '0';
  public $domesticInstitutions = '0';
  public $domesticCollaborators = '0';
  public $foreignInstitutions = '0';
  public $foreignCollaborators = '0';

  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * Sets type of object.
   *
   * @param string $type
   *   Name to set name value as.
   */
  public function setName($type) {
    $this->type = $type;
  }

  /**
   * Placement of data from query in Participant data object.
   *
   * @param array $data_row
   *   Data array query.
   */
  public function groupData(array $data_row) {
    if (isset($data_row['local'])) {
      $this->localCollaborators += count($data_row['local']);
    }
    if (isset($data_row['domestic'])) {
      $this->domesticCollaborators += count($data_row['domestic']);
    }
    if (isset($data_row['foreign'])) {
      $this->foreignCollaborators += count($data_row['foreign']);
    }
  }

  public function groupInst(array $institution_array) {
    if (isset($institution_array['local'])) {
      $unique=array_unique($institution_array['local']);
      $this->localInstitutions=count($unique);
      
    }
    if (isset($institution_array['domestic'])) {
      $unique=array_unique($institution_array['domestic']);
      $this->domesticInstitutions=count($unique);
    }
    if (isset($institution_array['foreign'])) {
      $unique=array_unique($institution_array['foreign']);
      $this->foreignInstitutions=count($unique);
    }

  }


  public function groupInstTot(array $institution_array) {
    if (isset($institution_array['local'])) {
      $unique=array_unique($institution_array['local']);
      
      $this->localInstitutions += count($unique);
    }
    if (isset($institution_array['domestic'])) {
      $unique=array_unique($institution_array['domestic']);
      $this->domesticInstitutions += count($unique);
    }
    if (isset($institution_array['foreign'])) {
      $unique=array_unique($institution_array['foreign']);
      $this->foreignInstitutions += count($unique);
    }

  }


}


