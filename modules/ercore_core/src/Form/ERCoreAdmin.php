<?php

namespace Drupal\ercore_core\Form;

/**
 * @file
 * Contains Drupal\ercore\Form\ERCoreAdmin.
 */

use Drupal\Core\Form\ConfigFormBase;
use Drupal\ercore\ErcoreStartDate;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ERCoreAdmin.
 *
 * Defines ERCore admin page.
 *
 * @package Drupal\ercore\Form
 */
class ERCoreAdmin extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ercore.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ercore_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ercore.settings');
    $form['ercore_epscor_start'] = [
      '#title' => t('Grant Start Date'),
      '#description' => $this->t('The start date for form processing and exports.'),
      '#type' => 'date',
      '#default_value' => $config->get('ercore_epscor_start'),
    ];
    $form['ercore_epscor_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('EPSCoR Grant Number'),
      '#description' => $this->t('EPSCoR or NSF Grant Number for display and exported forms.'),
      '#default_value' => $config->get('ercore_epscor_number'),
    ];
    $form['ercore_reporting_month'] = [
      '#type' => 'select',
      '#title' => $this->t('Reporting period start month'),
      '#description' => $this->t('Reporting month may differ from start month above.'),
      '#default_value' => $config->get('ercore_reporting_month'),
      '#options' => [
        '01' => 'January',
        '02' => 'February',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December',
      ],
    ];
    $form['ercore_grant_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Grant Type'),
      '#description' => $this->t('Grant type defines time period involved.'),
      '#default_value' => $config->get('ercore_grant_type'),
      '#options' => [
        '5' => 'Tier One',
        '3' => 'Tier Two',
      ],
    ];
    $form['ercore_grant_extension'] = [
      '#type' => 'select',
      '#title' => $this->t('Grant Extension'),
      '#description' => $this->t('Adjusts grant end date by extension.'),
      '#default_value' => $config->get('ercore_grant_extension'),
      '#options' => [
        '0' => 'No Extension',
        '6' => 'Six month extension',
        '12' => 'One year extension',
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $end = explode('-', $form_state->getValue('ercore_epscor_start'));
    $end[0] += $form_state->getValue('ercore_grant_type');
    $end[1] += $form_state->getValue('ercore_grant_extension');
    if ($end[1] >= 13) {
      $end[0] += 1;
      $end[1] = $end[1] - 12;
    }
    $end[1] = str_pad($end[1], 2, '0', STR_PAD_LEFT);
    $end = implode('-', $end);
    $this->config('ercore.settings')
      ->set('ercore_epscor_number', $form_state->getValue('ercore_epscor_number'))
      ->set('ercore_epscor_start', $form_state->getValue('ercore_epscor_start'))
      ->set('ercore_epscor_end', $end)
      ->set('ercore_grant_type', $form_state->getValue('ercore_grant_type'))
      ->set('ercore_grant_extension', $form_state->getValue('ercore_grant_extension'))
      ->set('ercore_reporting_month', $form_state->getValue('ercore_reporting_month'))
      ->save();
    self::resetFilters();

    // retrieve views info
    $config = $this->config('ercore.settings');
    // $test = $this->config('views.view.ercore_summary_of_proposals_and_grants_totals');
    $min1 = \Drupal::config('views.view.ercore_summary_of_proposals_and_grants_totals')->get('display.default.display_options.filters.field_ercore_pp_proposal_submit_value.group_info.group_items.1.value.min');
    $config_Factory = \Drupal::configFactory()->getEditable('views.view.ercore_summary_of_proposals_and_grants_totals');
  
    // get epscor start date string
    $epscor_start_date_str = $config->get('ercore_epscor_start');
    // get reporting month string
    $rep_month = $config->get('ercore_reporting_month');
    // parse epscore start date string 
    $epscor_start_date = date_parse($epscor_start_date_str);
    
    $separator = "-";
    // assign date components to variables 
    $year = strval($epscor_start_date['year']);
    $month = strval($epscor_start_date['month']);
    $day = strval($epscor_start_date['day']);

    // create new string for reporting date 
    $date_str = $year.$separator.$rep_month.$separator.$day;
    // convert reporting date string to date object
    $date_strtotime = strtotime($date_str);


    function compareDates($report_date, $epscor_start){
      $separator = "-";
      $r_parsed = date_parse($report_date);
      // assign date components to variables 
      $r_year = strval($r_parsed['year']);
      $r_month = strval($r_parsed['month']);
      $r_day = '01';
      $r_date = $r_year.$separator.$r_month.$separator.$r_day;
      if($report_date >= $epscor_start){
        // dpm('this is very hank');
          return date('Y-m-d',strtotime($r_date));
      }
      else{
        // dpm('this is unhank');
        //add a year
        return date('Y-m-d',strtotime($r_date. ' + 1 years'));
      }
    }

    $newDate = compareDates($date_str, $epscor_start_date_str);
    
    $views = ['views.view.ercore_summary_of_proposals_and_grants_totals', 'views.view.publication_totals'];
    $group_fields_proposal = ['field_ercore_pp_proposal_submit_value','field_ercore_pp_award_start_value'];
    $group_fields_pubs = ['field_ercore_pb_submit_date_value', 'field_ercore_pb_date_value'];

    foreach($views as $view){
      if($view === 'views.view.ercore_summary_of_proposals_and_grants_totals'){
        $group_fields = $group_fields_proposal;
      }
      else{
        $group_fields = $group_fields_pubs;
      }
      foreach($group_fields as $key){
        for ($x = 0; $x <= 5; $x++) {
  
          $y=$x+1;
          $add_year_str_x = '+'.$x.' years';
          $add_year_str_y = '+'.$y.' years';
          $sub_day_str = '-1 days' ;
          $min = strtotime($newDate. $add_year_str_x);
          $min_str = date('Y-m-d',$min);
          $max = date('Y-m-d', strtotime($newDate. $add_year_str_y));
          $max_str = date('Y-m-d', strtotime($max. $sub_day_str));
          
          if($x !== 5){
            \Drupal::configFactory()->getEditable($view)
              ->set('display.default.display_options.filters.'.$key.'.group_info.group_items.'.$y.'.value.min', $min_str)
              ->set('display.default.display_options.filters.'.$key.'.group_info.group_items.'.$y.'.value.max', $max_str)
              ->save();
          }
          else {
            \Drupal::configFactory()->getEditable($view)
              ->set('display.default.display_options.filters.'.$key.'.group_info.group_items.'.$y.'.value.value', $min_str)
              ->save();
          }
        }
      }
    }

    /* sets values for undergrad, grad, and doctoral graduation views */

    $grad_views = ['views.view.undergraduate_graduation_dates', 'views.view.graduate_graduation_dates', 'views.view.doctorate_graduation_dates'];
    // get epscor start date string
    $epscor_start_date_str = $config->get('ercore_epscor_start');
    // get reporting month string
    $reporting_start_month = $config->get('ercore_reporting_month');
    // parse epscore start date string 
    $epscor_start_date = date_parse($epscor_start_date_str);

    $separator = "-";
    // assign date components to variables 
    $year = strval($epscor_start_date['year']);
    $month = strval($epscor_start_date['month']);
    $day = strval($epscor_start_date['day']);

    foreach($grad_views as $view){
      if ($view == 'views.view.undergraduate_graduation_dates'){
        $class='under';
      }
      elseif($view == 'views.view.graduate_graduation_dates'){
        $class='master';
      }
      else{
        $class='doc';
      }

      // ! data_export_1 does not have date filter for all three views
      for ($x = 1; $x <= 6; $x++) {

        $add_years_min = $x-1;
        $add_years_max = $x;
        $date_min = $epscor_start_date_str;
        $date_max = date('Y-m-d',strtotime($year.$separator.$reporting_start_month.$separator.'1'.' + '.$add_years_max.' years'.' -1 days'));
        if($x != 1){
          $date_min = date('Y-m-d',strtotime($year.$separator.$reporting_start_month.$separator.'1'.' + '.$add_years_min.' years'));
        }
        // exports -------------------------------------------------------------------------------------------------------------------------
        if($x != 6){
          \Drupal::configFactory()->getEditable($view)
            ->set('display.data_export_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_ant_value.value.min', $date_min)
            ->set('display.data_export_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_ant_value.value.max', $date_max)
            ->save();

          \Drupal::configFactory()->getEditable($view)
            ->set('display.data_export_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_act_value.value.min', $date_min)
            ->set('display.data_export_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_act_value.value.max', $date_max)
            ->save();
        }
        if($x == 6 && $class != 'under'){

          \Drupal::configFactory()->getEditable($view)
            ->set('display.data_export_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_ant_value.value.min', $epscor_start_date_str)
            ->set('display.data_export_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_ant_value.value.max', $date_max)
            ->save();

          \Drupal::configFactory()->getEditable($view)
            ->set('display.data_export_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_act_value.value.min', $epscor_start_date_str)
            ->set('display.data_export_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_act_value.value.max', $date_max)
            ->save();
        }
        // exports -------------------------------------------------------------------------------------------------------------------------
        
        // pages ---------------------------------------------------------------------------------------------------------------------------
        
        $overall_min = $epscor_start_date_str;
        $overall_max = date('Y-m-d',strtotime($year.$separator.$reporting_start_month.$separator.'1'.' + 5 years'.' -1 days'));
        $page = $x + 2; 
        
        if($page != 8){

          \Drupal::configFactory()->getEditable($view)
            ->set('display.page_'.$page.'.display_options.filters.field_ercore_user_'.$class.'_ant_value.value.min', $date_min)
            ->set('display.page_'.$page.'.display_options.filters.field_ercore_user_'.$class.'_ant_value.value.max', $date_max)
            ->save();

          \Drupal::configFactory()->getEditable($view)
            ->set('display.page_'.$page.'.display_options.filters.field_ercore_user_'.$class.'_act_value.value.min', $date_min)
            ->set('display.page_'.$page.'.display_options.filters.field_ercore_user_'.$class.'_act_value.value.max', $date_max)
            ->save();
        }
        if($class == 'doc'){
          if($x <= 2){
            \Drupal::configFactory()->getEditable($view)
              ->set('display.page_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_ant_value.value.min', $overall_min)
              ->set('display.page_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_ant_value.value.max', $overall_max)
              ->save();

            \Drupal::configFactory()->getEditable($view)
              ->set('display.page_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_act_value.value.min', $overall_min)
              ->set('display.page_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_act_value.value.max', $overall_max)
              ->save();
          }
        }
        if($class == 'master'){
          if($x == 2){

            \Drupal::configFactory()->getEditable($view)
              ->set('display.page_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_ant_value.value.min', $overall_min)
              ->set('display.page_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_ant_value.value.max', $overall_max)
              ->save();

            \Drupal::configFactory()->getEditable($view)
              ->set('display.page_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_act_value.value.min', $overall_min)
              ->set('display.page_'.$x.'.display_options.filters.field_ercore_user_'.$class.'_act_value.value.max', $overall_max)
              ->save();
          }
        }
        // pages ---------------------------------------------------------------------------------------------------------------------------
      }
    }
    
  }

  /**
   * Reset date filters to use new dates.
   */
  public function resetFilters() {
    $filter = \Drupal::service('user.private_tempstore')
      ->get('ercore_core');
    $filter->set('ercore_chosen_range', 0);
    $filter->set('ercore_filter_start', ErcoreStartDate::startString());
    $filter->set('ercore_filter_end', ErcoreStartDate::endString());
    $messenger = \Drupal::messenger();
    $messenger->addMessage('Filters have been reset after base date update.');
  }

}
