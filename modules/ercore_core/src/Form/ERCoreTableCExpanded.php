<?php

namespace Drupal\ercore_core\Form;

/**
 * @file
 * Contains Drupal\ercore\Form\ERCoreTableC.
 */

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ercore_core\ErcoreCollaborationBuildExpanded;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class ERCoreTableC.
 *
 * Defines ERCore Table C.
 *
 * @package Drupal\ercore\Form
 */
class ERCoreTableCExpanded extends FormBase
{

  /**
   * {@inheritdoc}
   */
    public function getFormId()
    {
        return 'ERCoreTableC';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $url = Url::fromRoute('ercore_core.collaborations_export');
        $link = Link::fromTextAndUrl('Download NSF Table C.', $url);
        $form['#attached']['library'][] = 'ercore_core/ercore-core-exports.library';
        $cat=$_GET["cat"];
        $type=$_GET["type"];
        $nodetype=$_GET["nodetype"];
        $types = array(
          "local" => "Local",
          "domestic" => "Domestic",
          "foreign" => "Foreign",
        );

        $cats2 = array(
          "academic_research_institutions" => "Academic Research Institutions",
          "primarily_undergraduate_institutions" => "Primarily Undergraduate Institutions",
          "historically_black_colleges_and_universities" => "Historically Black Colleges and Universities",
          "hispanic_serving_institutions" => "Hispanic Serving Institutions",
          "tribal_colleges_and_universities" => "Tribal Colleges and Universities",
          "national_laboratories" => "National Laboratories",
          "industry" => "Industry",
          "none_other" => "None/Other",
          "totals" => "Totals"
        );

        if ($_GET["nodetype"]=='institutions') {
            $form['data_table'] = [
        '#type' => 'fieldset',
        '#title' => t($types[$type]." > ".$cats2[$cat]. ' > Institutions'),
        '#open' => true,
      ];
        } else {
            $form['data_table'] = [
        '#type' => 'fieldset',
        '#title' => t($types[$type]." > ".$cats2[$cat]. ' > Collaborators'),
        '#open' => true,
      ];
        }

        $form['data_table']['description'] = [
      '#markup' => self::formatResults(),
    ];

        $form['export_link'] = [
      '#markup' => '<p class="epscor-download">' . $link->toString() . '</p>',
    ];
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // We don't use this, but the interface requires us to implement it.
    }

    /**
     * Format Results.
     */
    public function formatResults()
    {
        $cat=$_GET["cat"];
        $type=$_GET["type"];
        $nodetype=$_GET["nodetype"];
        $cats = array(
      "academic_research_institutions" => "academic",
      "primarily_undergraduate_institutions" => "undergrad",
      "historically_black_colleges_and_universities" => "black",
      "hispanic_serving_institutions" => "hispanic",
      "tribal_colleges_and_universities" => "tribal",
      "national_laboratories" => "lab",
      "industry" => "industry",
      "none_other" => "none",
      "totals" => "totals"
    );

        $data = ErcoreCollaborationBuildExpanded::getData($cats[$cat], $type, $nodetype);
        $results = '';
        if ($nodetype=='institutions') {
            foreach ($data as $key => $value) {
                // $results .='<h2>'.$key.'</h2>';
                $results .= '<a href="/node/' . $data[$key]['id'] . '">' . '<h2>'.$key.'</h2>' . '</a>';
                $results .= '<h4 class="indent30">Collaborations:</h4>';
                foreach ($data[$key]['collabs'] as $key => $value) {
                    $results .= '<div class="indent60"><a  href="/node/' . $value . '">' .$key . '</a></div>';
                }

            }
        }
        if ($nodetype=='collaborator') {
            $num=1;
            foreach ($data as $key => $value) {
                $results .='<h2>'.$key.'</h2>';
                // $results .= '<a href="/node/' . $data[$key]['id'] . '">' . '<h2>'.$key.'</h2>' . '</a>';
                $results .= '<span class="indent30">Institution:'.'<a href="/node/' . $data[$key]['id'] . '">' . $data[$key]['institution']. '</a></span>';
                $results .= '<h4 class="indent30">Collaborations:</h4>';
                foreach ($data[$key]['collabs'] as $key => $value) {
                    $results .= '<div class="indent60"><a  href="/node/' . $value . '">' .$key . '</a>  Count Number:'.$num.'</div>';
                    $num++;
                }
            }

            
        }
    
        if ($nodetype=='collaborator2') {
            $data = ErcoreCollaborationBuildExpanded::getData($cats[$cat], $type, 'collaborator');
            $num=1;
            foreach ($data as $key => $value) {
                foreach ($data[$key]['collabs'] as $key2 => $value2) {
                    $results .='<h2>'.$key.'</h2>';
                    $results .= '<span class="indent30">Institution:'.'<a href="/node/' . $data[$key]['id'] . '">' . $data[$key]['institution']. '</a></span><div></div>';
                    $results .= '<span class="indent30">Collaboration:<a  href="/node/' . $value2 . '">' .$key2 . '</a></span>';
      
                    $num++;
                }
            }
        }

       
        return $results;
    }
}
