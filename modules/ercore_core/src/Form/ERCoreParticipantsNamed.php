<?php

namespace Drupal\ercore_core\Form;

/**
 * @file
 * Contains Drupal\ercore\Form\ERCoreParticipantsNamed.
 */

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\ercore_core\ErcoreParticipantNamedBuild;

/**
 * Class ERCoreParticipantsNamed.
 *
 * Defines ERCore Table B.
 *
 * @package Drupal\ercore\Form
 */
class ERCoreParticipantsNamed extends FormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'ERCoreParticipantsNamed';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $url = Url::fromRoute('ercore_core.participants_export');
        $link = Link::fromTextAndUrl('Download NSF Table B.', $url);
        $data = $this->formatResults();
        $form['#attached']['library'][] = 'ercore_core/ercore-core-exports.library';
        $form['date_filter'] = \Drupal::formBuilder()->getForm('Drupal\ercore_core\Form\ERCoreDateFilter');
        $form['data_table'] = [
            '#type' => 'fieldset',
            '#title' => t('Participants with Names'),
            '#open' => true,
        ];
        $form['data_table']['description'] = [
            '#markup' => $data,
        ];
        $form['export_link'] = [
            '#markup' => '<p class="epscor-download">' . $link->toString() . '</p>',
        ];
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // We don't use this, but the interface requires us to implement it.
    }

    /**
     * Format Results.
     */
    public function formatResults()
    {
        $data = ErcoreParticipantNamedBuild::getData();
        $results = '';
        foreach ($data as $institution) {

            $results .= '<div ><h3>' . $institution['name'] . '</h3></div>';

            foreach ($institution as $k => $v) {
                if ($k != 'name') {
                    $results .= '<h5 class="indent30">' . $k . '</h5>';
                    foreach ($institution[$k] as $key => $value) {
                        if (!empty($key)) {
                            $results .= '<div class="indent60">' . $key . ':&nbsp;';
                            foreach ($institution[$k][$key] as $key2 => $value2) {
                                $results .= '<a href="/user/' . $value2[1] . '">' . $value2[0] . '</a>,&nbsp;';
                            }
                            $results = substr($results, 0, -7);
                            $results .= '</div>';
                        }
                    }
                }
            }
        }
        return $results;
    }

}
