<?php

namespace Drupal\ercore_core;

use Drupal\ercore\ErcoreStartDate;

/**
 * Class ErcoreCollaborationBuildNames.
 *
 * @package Drupal\ercore_core
 */
class ErcoreCollaborationBuildExpanded
{

  /**
   * Data types which form row headers.
   *
   * @return array
   *   Return array of data types.
   */
    public static function dataTypes()
    {
        return [
      'academic' => 'Academic Research Institutions',
      'undergrad' => 'Primarily Undergraduate Institutions',
      'black' => 'Historically Black Colleges and Universities',
      'hispanic' => 'Hispanic Serving Institutions',
      'tribal' => 'Tribal Colleges and Universities',
      'lab' => 'National Laboratories',
      'industry' => 'Industry',
      'none' => 'None/Other',
      'totals' => 'Totals',
    ];
    }

    /**
     * Builds data array for participant data block.
     *
     * @return array
     *   Returns array of objects.
     */
    public static function buildDataArray()
    {
        $dataArray = [];
        foreach (self::dataTypes() as $key => $type) {
            $dataArray[$key] = new ErcoreCollaboration();
            $dataArray[$key]->setName($type);
        }
        return $dataArray;
    }

    /**
     * Build Node ID array.
     *
     * @return array
     *   Array of User IDs.
     */
    public static function getNodeIds()
    {
        $query = \Drupal::entityQuery('node')
      ->condition('type', 'ercore_collaboration')
      ->condition('status', 1);
        return $query->execute();
    }

    /**
     * Build data object.
     *
     * @return array
     *   Array of Users.
     */
    public static function getNodes()
    {
        $ids = self::getNodeIds();
        $nodes = [];
        $nodesnames=[];
        $dates = ercore_get_filter_dates();

        foreach ($ids as $id) {
            $node = \Drupal::entityTypeManager()->getStorage('node')->load($id);
            // $options = $node->field_text_list->getSetting('allowed_values');
      
            $entityManager = \Drupal::service('entity_field.manager');
            $fields = $entityManager->getFieldStorageDefinitions('node');
            $options = options_allowed_values($fields['field_ercore_inst_category']);
            $types = options_allowed_values($fields['field_ercore_inst_type']);
      
      

            $titlename=$node->get('title')->getValue()[0]['value'];
           
            if (!$node->get('field_ercore_collaboration_start')->isEmpty()) {
                $collaboration_start = $node->get('field_ercore_collaboration_start')->getValue();
                if (!$node->get('field_ercore_collaboration_end')->isEmpty()) {
                    $end = $node->get('field_ercore_collaboration_end')->getValue();
                    $collaboration_end = ErcoreStartDate::dateArgumentToUnix($end[0]['value']);
                } else {
                    $collaboration_end = ErcoreStartDate::endUnix();
                }
                $starttime=ErcoreStartDate::dateArgumentToUnix($collaboration_start[0]['value']);
                $endtime=$collaboration_end;
                if (($starttime <= $dates['end'] && $endtime >= $dates['start']) || ($starttime <= $dates['end'] && empty($endtime))) {
                    if (!$node->get('field_ercore_cn_collaborator')->isEmpty()) {
                        $nodes[$id] = [
            'start' => ErcoreStartDate::dateArgumentToUnix($collaboration_start[0]['value']),
            'end' => $collaboration_end,
            'count' => 0,
          ];
                        $externals = $node->get('field_ercore_cn_collaborator')->getValue();
                        foreach ($externals as $external) {
                            $pid = $external['target_id'];
                            $type = '';
                            $category = '';
                            $title='';
                            $collaborator = \Drupal::entityTypeManager()
              ->getStorage('paragraph')
              ->load($pid);
            
                            if (!$collaborator->get('field_ercore_cr_inst')->isEmpty()) {
                                $institution = $collaborator->get('field_ercore_cr_inst')->entity;
             
                                if (!empty($institution)) {
                                    $instype=$types[$institution->get('field_ercore_inst_type')->value];
                                    $instcat=$options[$institution->get('field_ercore_inst_category')->value];

                                    $is_in_time_window=false;

                                    $is_in_time_window=true;

                                    if (!isset($nodesnames[$instype][$instcat]['titles'])) {
                                        $nodesnames[$instype][$instcat]['titles']=[];
                                    }
                                    $isinarr=false;
                                    foreach ($nodesnames[$instype][$instcat]['titles'] as $title) {
                                        if ($title['name']==$titlename) {
                                            $isinarr=true;
                                        }
                                    }
                                    if (!$isinarr && $is_in_time_window) {
                                        array_push($nodesnames[$instype][$instcat]['titles'], ['start' => $starttime,
                  'end' => $collaboration_end,
                  'name' => $titlename,
                  'id' => $id]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
   
      
        return $nodes;
    }

    /**
     * Filter data by date.
     *
     * @param bool $entire_range
     *   Use cumulative date or filtered.
     *
     * @return array
     *   Array of User IDs.
     */
    public static function filteredNodes($entire_range = false)
    {
        $dates = ercore_get_filter_dates();
        if ($entire_range === true) {
            $dates = ercore_get_project_filter_dates();
        }
        $filtered = [];
        $nodes = self::getNodes();
        foreach ($nodes as $nid => $node) {
            if (($node['start'] <= $dates['end'] && $node['end'] >= $dates['start'])
        || ($node['start'] <= $dates['end'] && empty($node['end']))) {
                $filtered[$nid] = $node;
            }
        }
        return $filtered;
    }

    /**
     * Counts external collaborators.
     *
     * @param bool $entire_range
     *   Use cumulative date or filtered.
     *
     * @return int
     *   Count of external collaborators.
     */
    public static function countExternals($entire_range = false)
    {
        $count = 0;
        $nodes = self::filteredNodes($entire_range);
        foreach ($nodes as $node) {
            $count += $node['count'];
        }
        return $count;
    }

    /**
     * Gets all Participant data.
     */
    public static function getData($cat, $type, $nodetype)
    {

        $instarray=array();
        if ($nodetype=='institutions') {
            $ids=self::getNodeIds();
            foreach ($ids as $id) {
                $node = \Drupal::entityTypeManager()->getStorage('node')->load($id);

                $dates = ercore_get_filter_dates();
    
                
                $end=strtotime($node->get('field_ercore_collaboration_end')->getValue()[0]['value']);
                $start=strtotime($node->get('field_ercore_collaboration_start')->getValue()[0]['value']);
         
                if (($start<= $dates['end'] && $end >= $dates['start'])
                 || ($start <= $dates['end'] && empty($end))) {
                   
                     foreach ($node->field_ercore_cn_collaborator as $collaborator) {
                         $referenced_collaborator = $collaborator->entity;
                         $instid=$referenced_collaborator->get('field_ercore_cr_inst')->getValue()[0]['target_id'];
                         $inst = \Drupal::entityTypeManager()->getStorage('node')->load($instid);
                         $thiscat=$inst->get('field_ercore_inst_category')->getValue()[0]['value'];
                         $thistype=$inst->get('field_ercore_inst_type')->getValue()[0]['value'];
                         if ($thiscat==$cat && $thistype==$type) {
                             $insttitle=$inst->get('title')->getValue()[0]['value'];
                             $inst->get('nid')->getValue()[0]['value'];
                             if (!array_key_exists($insttitle, $instarray)) {
                                 $tmparry=[];
                                 $tmparry['id']= $instid;
                                 $nodearr=array($node->get('title')->getValue()[0]['value'] => $node->get('nid')->getValue()[0]['value']);
                                 $tmparry['collabs']=$nodearr;
                                 $instarray[$insttitle]=$tmparry;
                             } else {
                                 if (!array_key_exists($node->get('title')->getValue()[0]['value'], $instarray[$insttitle]['collabs'])) {
                                     $instarray[$insttitle]['collabs'][$node->get('title')->getValue()[0]['value']]=$node->get('nid')->getValue()[0]['value'];
                                 }
                             }
                         }


                         if ($cat=='totals') {
                             if ($thistype==$type) {
                                 $insttitle=$inst->get('title')->getValue()[0]['value'];
                                 $inst->get('nid')->getValue()[0]['value'];
                                 if (!array_key_exists($insttitle, $instarray)) {
                                     $tmparry=[];
                                     $tmparry['id']= $instid;
                                     $nodearr=array($node->get('title')->getValue()[0]['value'] => $node->get('nid')->getValue()[0]['value']);
                                     $tmparry['collabs']=$nodearr;
                                     $instarray[$insttitle]=$tmparry;
                                 } else {
                                     if (!array_key_exists($node->get('title')->getValue()[0]['value'], $instarray[$insttitle]['collabs'])) {
                                         $instarray[$insttitle]['collabs'][$node->get('title')->getValue()[0]['value']]=$node->get('nid')->getValue()[0]['value'];
                                     }
                                 }
                             }
                         }
                     }
                 }
                 
              
            };
           
            return $instarray;
        };
        if ($nodetype=='collaborator') {
            $ids=self::getNodeIds();

            foreach ($ids as $id) {
                $node = \Drupal::entityTypeManager()->getStorage('node')->load($id);
                $dates = ercore_get_filter_dates();
                $end=strtotime($node->get('field_ercore_collaboration_end')->getValue()[0]['value']);
                $start=strtotime($node->get('field_ercore_collaboration_start')->getValue()[0]['value']);
         
                if (($start<= $dates['end'] && $end >= $dates['start'])
                 || ($start <= $dates['end'] && empty($end))) {
                     foreach ($node->field_ercore_cn_collaborator as $collaborator) {
                         $referenced_collaborator = $collaborator->entity;
                         $instid=$referenced_collaborator->get('field_ercore_cr_inst')->getValue()[0]['target_id'];
                         $collaborator_name=$referenced_collaborator->get('field_ercore_cr_name')->getValue()[0]['value'];
                    
                         $inst = \Drupal::entityTypeManager()->getStorage('node')->load($instid);
                         $thiscat=$inst->get('field_ercore_inst_category')->getValue()[0]['value'];
                         $thistype=$inst->get('field_ercore_inst_type')->getValue()[0]['value'];
                         if ($thiscat==$cat && $thistype==$type) {
                             $insttitle=$inst->get('title')->getValue()[0]['value'];
                             $inst->get('nid')->getValue()[0]['value'];
                             if (!array_key_exists($collaborator_name, $instarray)) {
                                 $tmparry=[];
                                 $tmparry['institution']= $insttitle;
                                 $tmparry['id']= $instid;
                                 $nodearr=array($node->get('title')->getValue()[0]['value'] => $node->get('nid')->getValue()[0]['value']);
                                 $tmparry['collabs']=$nodearr;
                                 $instarray[$collaborator_name]=$tmparry;
                             } else {
                                 $nodearr=array($node->get('title')->getValue()[0]['value'] => $node->get('nid')->getValue()[0]['value']);
                                 if (!array_key_exists($node->get('title')->getValue()[0]['value'], $instarray[$collaborator_name]['collabs'])) {
                                     $instarray[$collaborator_name]['collabs'][$node->get('title')->getValue()[0]['value']]=$node->get('nid')->getValue()[0]['value'];
                                 }
                             }
                         }
                         if ($cat=='totals') {
                             if ($thistype==$type) {
                                 $insttitle=$inst->get('title')->getValue()[0]['value'];
                                 $inst->get('nid')->getValue()[0]['value'];
                                 if (!array_key_exists($collaborator_name, $instarray)) {
                                     $tmparry=[];
                                     $tmparry['institution']= $insttitle;
                                     $tmparry['id']= $instid;
                                     $nodearr=array($node->get('title')->getValue()[0]['value'] => $node->get('nid')->getValue()[0]['value']);
                                     $tmparry['collabs']=$nodearr;
                                     $instarray[$collaborator_name]=$tmparry;
                                 } else {
                                     $nodearr=array($node->get('title')->getValue()[0]['value'] => $node->get('nid')->getValue()[0]['value']);
                                     if (!array_key_exists($node->get('title')->getValue()[0]['value'], $instarray[$collaborator_name]['collabs'])) {
                                         $instarray[$collaborator_name]['collabs'][$node->get('title')->getValue()[0]['value']]=$node->get('nid')->getValue()[0]['value'];
                                     }
                                 }
                             }
                         }
                     }
                 }




            };
            
            return $instarray;
        };
    }
}
