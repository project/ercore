<?php

namespace Drupal\ercore_core\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\views\Views;

/**
 * An example controller.
 */






class ERCoreDashboard extends ControllerBase {

  



  public function content() {
    \Drupal::service('page_cache_kill_switch')->trigger();

    $currentUser = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
    $component_array=$currentUser->get("field_ercore_user_component")->getValue();
    $component_users=[];
    foreach ($component_array as &$value) {
      
      $target_id=$value['target_id'];
      // $result = db_query('select entity_id from user__field_ercore_user_component where field_ercore_user_component_target_id ='.$target_id.';');
      
      $database = \Drupal::database();
      $query = $database->query("SELECT entity_id FROM user__field_ercore_user_component WHERE field_ercore_user_component_target_id =".$target_id);
      $result = $query->fetchAll();


      foreach ($result as $row) {
        $user_id=$row->entity_id;
        if (!in_array(user_id, $component_users)) {
          $component_users[] = $user_id;
        }
      }
    

    }


    $publications = \Drupal::service('renderer')->renderPlain(views_embed_view('dashboard_incomplete', 'unpublished_publications'));
    $proposals = \Drupal::service('renderer')->renderPlain(views_embed_view('dashboard_incomplete', 'pending_proposals'));
    $pending_patents = \Drupal::service('renderer')->renderPlain(views_embed_view('dashboard_incomplete', 'pending_patents'));
    $your_events = \Drupal::service('renderer')->renderPlain(views_embed_view('dashboard_incomplete', 'your_events'));
    $no_content="<div style='margin-bottom: 10px;'> &nbsp; &nbsp; &nbsp; <span style='color:grey'>No content found.</span></div>";
    if (strlen($your_events)<300){
      $your_events = $no_content;
    }
    $your_collaborations = \Drupal::service('renderer')->renderPlain(views_embed_view('dashboard_incomplete', 'your_collaborations'));
    if (strlen($your_collaborations)<300){
      $your_collaborations = $no_content;
    }
    $your_highlights = \Drupal::service('renderer')->renderPlain(views_embed_view('dashboard_incomplete', 'your_highlights'));
    if (strlen($your_highlights)<300){
      $your_highlights = $no_content;
    }
    $your_honors_and_awards = \Drupal::service('renderer')->renderPlain(views_embed_view('dashboard_incomplete', 'your_honors_and_awards'));
    if (strlen($your_honors_and_awards)<300){
      $your_honors_and_awards = $no_content;
    }
    $your_patents = \Drupal::service('renderer')->renderPlain(views_embed_view('dashboard_incomplete', 'your_patents'));
    if (strlen($your_patents)<300){
      $your_patents = $no_content;
    }
    
    $your_presentations = \Drupal::service('renderer')->renderPlain(views_embed_view('dashboard_incomplete', 'your_presentations'));
    if (strlen($your_presentations)<300){
      $your_presentations = $no_content;
    }

    $your_proposals_and_grants = \Drupal::service('renderer')->renderPlain(views_embed_view('dashboard_incomplete', 'your_proposals_and_grants'));
    if (strlen($your_proposals_and_grants)<300){
      $your_proposals_and_grants = $no_content;
    }

    $your_publications = \Drupal::service('renderer')->renderPlain(views_embed_view('dashboard_incomplete', 'your_publications'));
    if (strlen($your_publications)<300){
      $your_publications = $no_content;
    }

    $hooray="<div style='margin-bottom: 10px;'>&nbsp; &nbsp; &nbsp; <span>Nothing found! Hooray!</span></div>";
    if (strlen($proposals)<286){
      
      $proposals = $hooray;
    }

    if (strlen($publications)<300){
      $publications = $hooray;
    }
    if (strlen($pending_patents)<300){
      $pending_patents = $hooray;
    }

    

    $build = [
      
      '#markup' => $this->t('
      <div class="row">
       <div class="col-md-12">
        <div style="text-align: center;"></div>  
        <div class="row" style="border-style: inset;
        border-color: #2e5d62;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px;">
        <div class="col-md-6" style="">
         <div class="row">
            <div style="background-color: #125d66;
            color: white;
            border-top-left-radius: 5px;
            text-align: center;
            font-size: larger;">Your Content in Progress</div>
           <div class="col-md-12"><div style="font-weight: bold;">Publications (Status Not Published)</div>
           <div>'.$publications.'</div>
           </div>
           <div class="col-md-12"><div style="font-weight: bold;">Proposals (Submitted or Pending) </div>
           <div>'.$proposals.'</div>
           </div>
           <div class="col-md-12"><div style="font-weight: bold;">Patents (Missing Award Date) </div>
           <div>'.$pending_patents.'</div>
           </div>
         </div>
        </div>
        <div class="col-md-6" style="border-left-style: solid;">
         <div class="row">
            <div style="background-color: #125d66;
            color: white;
            border-top-right-radius: 5px;
            text-align: center;
            font-size: larger;">Your Completed Content</div>
            <div class="col-md-12"><div style="font-weight: bold;">Events</div>
            <div>'.$your_events.'</div>
            </div>
            <div class="col-md-12"><div style="font-weight: bold;">Collaborations</div>
            <div>'.$your_collaborations.'</div>
            </div>
            <div class="col-md-12"><div style="font-weight: bold;">Highlights</div>
            <div>'.$your_highlights.'</div>
            </div>
            <div class="col-md-12"><div style="font-weight: bold;">Your Honors and Awards</div>
            <div>'.$your_honors_and_awards.'</div>
            </div>
            <div class="col-md-12"><div style="font-weight: bold;">Your Patents</div>
            <div>'.$your_patents.'</div>
            </div>
            <div class="col-md-12"><div style="font-weight: bold;">Your Presentations</div>
            <div>'.$your_presentations.'</div>
            </div>
            <div class="col-md-12"><div style="font-weight: bold;">Your Proposals and Grants</div>
            <div>'.$your_proposals_and_grants.'</div>
            </div>
            <div class="col-md-12"><div style="font-weight: bold;">Your Publications</div>
            <div>'.$your_publications.'</div>
            </div>
         </div>
        </div>
      </div></div>
  
    </div>'),
    ];
    


    
    return $build;
  }


  


}